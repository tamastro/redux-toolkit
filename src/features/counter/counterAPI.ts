import axios from "axios"; 
import instance from "../../axiosCofig";

// A mock function to mimic making an async request for data
export function fetchCount(amount = 1) {
  return new Promise<{ data: number }>((resolve) =>
    setTimeout(() => resolve({ data: amount }), 500)
  );
}

export const fetchToken = async () => {
  const request = await instance({
    method: 'POST',
    url: `/api/login/token`,
    data: {
      username: 'riyani.wardani@yopmail.com',
      password: 'password123'
    }
  })
  
  localStorage.setItem('token', request.data.access_token);
  localStorage.setItem('refeshToken', request.data.refresh_token);
  return request.data
}

const fetchRefreshToken = async () => {
  const request = await instance({
    method: 'POST',
    url: `/api/login/token-refresh`,
    data: {
      client_id: "cloe-ext-login",
      grand_type: "refresh_token",
      refresh_token: localStorage.getItem('refeshToken')
    }
  })

  localStorage.setItem('token', JSON.stringify(request.data.access_token));
  localStorage.setItem('refeshToken', JSON.stringify(request.data.refresh_token));
  return request.data
}

export const fetchData = async (token: any) => {
  try {
    const request = await 
      instance ({
      method: 'GET',
      url: `/api/lookup/customer-type`,
      headers: { Authorization: `Bearer ${token}` },
    })
    
    return request.data
  } catch (error) {
    fetchRefreshToken()
    fetchData(localStorage.getItem('token'))
  }

}